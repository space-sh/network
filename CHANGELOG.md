# Space Module change log - network

## [2.0.0 - 2021-01-19]

+ Add `NETWORK_LOCAL_IP`

* Rename `NETWORK_PORT_BUSY` to `NETWORK_PORT_FREE`


## [1.0.0 - 2020-02-18]

+ Initial version
